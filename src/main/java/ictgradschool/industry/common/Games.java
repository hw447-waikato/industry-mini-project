package ictgradschool.industry.common;

import java.util.Scanner;

public class Games {
    public static void main(String[] args) {
        // Creating an Array
        int[] sysNums = new int[4];
        //Place random number
        for (int i = 0; i < sysNums.length; i++) {
            //Generate a random number from 0 to 9
            sysNums[i] = (int) (Math.random() * 10);
            for (int j = 0; j < i; j++) {
                //Reassignment
                if (sysNums[i] == sysNums[j]) {
                    i--;
                    break;
                }
            }
        }
        System.out.println("Computer generated numbers: ****");

        Scanner input = new Scanner(System.in);
        char[] inputChars;
        int attempts = 7;

        // Player chooses a password
        System.out.println("Please choose a 4-digit password (each digit between 0 and 9):");
        String playerPassword = input.nextLine();
        while (!isValidPassword(playerPassword)) {
            System.out.println("Invalid password! Please choose a 4-digit password:");
            playerPassword = input.nextLine();
        }
        System.out.println("Player's password is set!");

        // Game loop
        while (attempts > 0) {
            // Player's turn
            System.out.println("\nYou have " + attempts + " attempts left.");
            System.out.println("Please enter your guess (4-digit number):");
            String playerGuess = input.next();
            while (!isValidPassword(playerGuess)) {
                System.out.println("Invalid guess! Please enter a 4-digit number:");
                playerGuess = input.next();
            }

            // Evaluate player's guess
            String playerResult = evaluateGuess(playerGuess, sysNums);
            System.out.println(playerResult);
            if (playerResult.equals("4A0B")) {
                System.out.println("Player wins!");
                break;
            }

            attempts--;

            if (attempts == 0) {
                System.out.println("You lost! The correct number was: " + sysNums[0] + sysNums[1] + sysNums[2] + sysNums[3]);
                break;
            }

            // Computer's turn
            String computerGuess = generateComputerGuess();
            String computerResult = evaluateGuess(computerGuess, playerPassword);
            System.out.println("Computer guesses: " + computerGuess);
            System.out.println("Computer's result: " + computerResult);
            if (computerResult.equals("4A0B")) {
                System.out.println("Computer wins!");
                break;
            }
        }
    }

    // Method to check if the player's chosen password is valid
    private static boolean isValidPassword(String password) {
        return password.matches("[0-9]{4}");
    }

    // Method to generate computer's guess
    private static String generateComputerGuess() {
        StringBuilder guess = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            guess.append((int) (Math.random() * 10));
        }
        return guess.toString();
    }

    // Method to evaluate a guess and return the result
    private static String evaluateGuess(String guess, String password) {
        int countA = 0;
        int countB = 0;

        for (int i = 0; i < password.length(); i++) {
            char digit = guess.charAt(i);
            if (digit == password.charAt(i)) {
                countA++;
            } else if (password.contains(String.valueOf(digit))) {
                countB++;
            }
        }

        return countA + "A" + countB + "B";
    }

    // Method to evaluate player's guess and return the result
    private static String evaluateGuess(String guess, int[] sysNums) {
        int countA = 0;
        int countB = 0;

        for (int i = 0; i < sysNums.length; i++) {
            for (int j = 0; j < guess.length(); j++) {
                if (sysNums[i] == guess.charAt(j) - '0') {
                    if (i == j) {
                        countA++;
                    } else {
                        countB++;
                    }
                }
            }
        }

        return countA + "A" + countB + "B";
    }
}
